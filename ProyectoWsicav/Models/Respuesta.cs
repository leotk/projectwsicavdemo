﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoWsicav.Models
{
    public class Respuesta
    {
        public string mensaje { get; set; }
        public bool estado { get; set; }
        public dynamic resultado { get; set; }
    }
}
