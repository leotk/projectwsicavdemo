﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoWsicav.Models
{
    [Table("Tbl_Conductor")]
    public partial class TblConductor
    {
        [Key]
        public int Id { get; set; }
        [StringLength(50)]
        public string Codigo { get; set; }
        public int? TipoDocumentoIdentidadId { get; set; }
        public int? NumDocumento { get; set; }
        [StringLength(50)]
        public string Apellidos { get; set; }
        [StringLength(50)]
        public string Nombres { get; set; }
        public int? Telefono { get; set; }
        [StringLength(100)]
        public string Direccion { get; set; }
        [StringLength(100)]
        public string Email { get; set; }
        public int? TipoLicenciaId { get; set; }

        [ForeignKey(nameof(TipoDocumentoIdentidadId))]
        [InverseProperty(nameof(TblTipoDocumentoIdentidad.TblConductor))]
        public virtual TblTipoDocumentoIdentidad TipoDocumentoIdentidad { get; set; }
        [ForeignKey(nameof(TipoLicenciaId))]
        [InverseProperty(nameof(TblTipoLicencia.TblConductor))]
        public virtual TblTipoLicencia TipoLicencia { get; set; }
    }
}
