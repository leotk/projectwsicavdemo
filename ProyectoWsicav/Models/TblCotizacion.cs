﻿using ProyectoWsicav.ViewObject;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoWsicav.Models
{
    [Table("Tbl_Cotizacion")]
    public partial class TblCotizacion
    {
        public TblCotizacion()
        {
        }

        public TblCotizacion(VCotizacion coti) //Empleado para guardar
        {
            SolicitudId = coti.SolicitudId;
            FechaCotizacion = coti.Fecha;
            NumeCotizacion = coti.NumeCotizacion;
            Monto = coti.Monto;
        }

        [Key]
        public int Id { get; set; }
        public int SolicitudId { get; set; }
        [Column(TypeName = "datetime"), DataType(DataType.Date)]
        public DateTime? FechaCotizacion { get; set; }
        [StringLength(50)]
        public string NumeCotizacion { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? Monto { get; set; }
        [StringLength(50)]
        public string Documentonumero { get; set; }
        [StringLength(50)]
        public string Documentonombre { get; set; }
        public byte[] Documento { get; set; }

        [ForeignKey(nameof(SolicitudId))]
        [InverseProperty(nameof(TblSolicitud.TblCotizacion))]
        public virtual TblSolicitud Solicitud { get; set; }
    }
}
