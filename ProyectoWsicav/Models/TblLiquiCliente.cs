﻿using ProyectoWsicav.ViewObject;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoWsicav.Models
{
    [Table("Tbl_LiquiCliente")]
    public partial class TblLiquiCliente
    {
        public TblLiquiCliente() { }

        public TblLiquiCliente(VLicliente lc) //PARA REGISTRAR GUARDAR
        {
            SolicitudId = lc.SolicitudId;
            FechaLcliente = lc.FechaLcliente;
            FormaPago = lc.FormaPago;
            Banco = lc.Banco;
            Titular = lc.Titular;
            NumeTarjeta = lc.NumeTarjeta;
            NumeVouch = lc.NumeVouch;
            Monto = lc.Monto;
        }

        [Key]
        public int Id { get; set; }
        public int SolicitudId { get; set; }
        [Column("FechaLCliente", TypeName = "datetime"),DataType(DataType.Date)]
        public DateTime? FechaLcliente { get; set; }
        [StringLength(50)]
        public string FormaPago { get; set; }
        [StringLength(50)]
        public string Banco { get; set; }
        [StringLength(100)]
        public string Titular { get; set; }
        [StringLength(16)]
        public string NumeTarjeta { get; set; }
        [StringLength(20)]
        public string NumeVouch { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? Monto { get; set; }
        [StringLength(50)]
        public string Documentonumero { get; set; }
        [StringLength(50)]
        public string Documentonombre { get; set; }
        public byte[] Documento { get; set; }
        [Column("estado")]
        public bool? Estado { get; set; }

        [ForeignKey(nameof(SolicitudId))]
        [InverseProperty(nameof(TblSolicitud.TblLiquiCliente))]
        public virtual TblSolicitud Solicitud { get; set; }
    }
}
