﻿using ProyectoWsicav.ViewObject;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoWsicav.Models
{
    [Table("Tbl_LiquiProve")]
    public partial class TblLiquiProve
    {
        public TblLiquiProve() { }

        public TblLiquiProve(VLiprovee lp) //PARA GUARDAR REGISTRAR
        {
            SolicitudId = lp.SolicitudId;
            ProveedorId = lp.ProveedorId;
            FechaLprove = lp.FechaLprove;
            FormaPago = lp.FormaPago;
            Banco = lp.Banco;
            Titular = lp.Titular;
            NumeVaucher = lp.NumeVaucher;
            NumeTarjeta = lp.NumeTarjeta;
            PagoCliente = lp.PagoCliente;
            Monto = lp.Monto;
        }

        [Key]
        public int Id { get; set; }
        public int SolicitudId { get; set; }
        public int? ProveedorId { get; set; }
        [Column("FechaLProve", TypeName = "datetime"), DataType(DataType.Date)]
        public DateTime? FechaLprove { get; set; }
        [StringLength(50)]
        public string FormaPago { get; set; }
        [StringLength(50)]
        public string Banco { get; set; }
        [StringLength(100)]
        public string Titular { get; set; }
        [StringLength(50)]
        public string NumeVaucher { get; set; }
        [StringLength(16)]
        public string NumeTarjeta { get; set; }
        public bool PagoCliente { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? Monto { get; set; }
        [StringLength(50)]
        public string Documentonumero { get; set; }
        [StringLength(50)]
        public string Documentonombre { get; set; }
        public byte[] Documento { get; set; }

        [ForeignKey(nameof(ProveedorId))]
        //[InverseProperty(nameof(TblProveedor.TblLiquiProve))]
        public virtual TblProveedor Proveedor { get; set; }
        [ForeignKey(nameof(SolicitudId))]
        [InverseProperty(nameof(TblSolicitud.TblLiquiProve))]
        public virtual TblSolicitud Solicitud { get; set; }
    }
}
