﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoWsicav.Models
{
    [Table("Tbl_Pasajeros")]
    public partial class TblPasajeros
    {
        [Key]
        public int Id { get; set; }
        public int? SolicitudId { get; set; }
        [StringLength(50),Required(ErrorMessage = "Nombres y apellidos requeridos")]
        public string NombresPasajeros { get; set; }
        public int? TipoPasajeroId { get; set; }
        [Required(ErrorMessage = "La edad es requerida")]
        [Range(1, 150, ErrorMessage = "Por favor ingrese una edad entre los 1 y 150 años")]
        public int? Edad { get; set; }
        public int? TipoDocumentoIdentidadId { get; set; }
        [StringLength(50), Required(ErrorMessage = "N° de documento requerido")]
        public string NumeroDucumento { get; set; }
        [StringLength(50)]
        public string Nacionalidad { get; set; }
        public bool Principal { get; set; }

        [ForeignKey(nameof(SolicitudId))]
        [InverseProperty(nameof(TblSolicitud.TblPasajeros))]
        public virtual TblSolicitud Solicitud { get; set; }
        [ForeignKey(nameof(TipoDocumentoIdentidadId))]
        //[InverseProperty(nameof(TblTipoDocumentoIdentidad.TblPasajeros))]
        public virtual TblTipoDocumentoIdentidad TipoDocumentoIdentidad { get; set; }
        [ForeignKey(nameof(TipoPasajeroId))]
        //[InverseProperty(nameof(TblTipoPasajero.TblPasajeros))]
        public virtual TblTipoPasajero TipoPasajero { get; set; }
    }
}
