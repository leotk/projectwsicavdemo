﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoWsicav.Models
{
    [Table("Tbl_Proveedor")]
    public partial class TblProveedor
    {
        public TblProveedor()
        {
            //TblLiquiProve = new HashSet<TblLiquiProve>();
            //TblReservasDetalle = new HashSet<TblReservasDetalle>();
        }

        [Key]
        public int Id { get; set; }
        [StringLength(20)]
        public string Codigo { get; set; }
        [StringLength(150)]
        public string Nombre { get; set; }
        public int? TipoDocumentoIdentidadId { get; set; }
        [StringLength(20)]
        public string Documento { get; set; }
        [StringLength(50)]
        public string Direccion { get; set; }
        [StringLength(50)]
        public string Telefono { get; set; }
        [StringLength(50)]
        public string Email { get; set; }

        [ForeignKey(nameof(TipoDocumentoIdentidadId))]
        [InverseProperty(nameof(TblTipoDocumentoIdentidad.TblProveedor))]
        public virtual TblTipoDocumentoIdentidad TipoDocumentoIdentidad { get; set; }
        //[InverseProperty("Proveedor")]
        //public virtual ICollection<TblLiquiProve> TblLiquiProve { get; set; }
        //[InverseProperty("Proveedor")]
        //public virtual ICollection<TblReservasDetalle> TblReservasDetalle { get; set; }
    }
}
