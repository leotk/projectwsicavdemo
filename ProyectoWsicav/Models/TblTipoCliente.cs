﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoWsicav.Models
{
    [Table("Tbl_TipoCliente")]
    public partial class TblTipoCliente
    {
        public TblTipoCliente()
        {
            TblCliente = new HashSet<TblCliente>();
        }

        [Key]
        public int Id { get; set; }
        [StringLength(50)]
        public string Descripcion { get; set; }

        [InverseProperty("TipoCliente")]
        public virtual ICollection<TblCliente> TblCliente { get; set; }
    }
}
