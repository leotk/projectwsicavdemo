﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoWsicav.Models
{
    [Table("Tbl_TipoDocumentoIdentidad")]
    public partial class TblTipoDocumentoIdentidad
    {
        public TblTipoDocumentoIdentidad()
        {
            TblCliente = new HashSet<TblCliente>();
            TblConductor = new HashSet<TblConductor>();
            TblGuia = new HashSet<TblGuia>();
            //TblPasajeros = new HashSet<TblPasajeros>();
            TblProveedor = new HashSet<TblProveedor>();
        }

        [Key]
        public int Id { get; set; }
        [StringLength(50)]
        public string Descripcion { get; set; }

        [InverseProperty("TipoDocumentoIdentidad")]
        public virtual ICollection<TblCliente> TblCliente { get; set; }
        [InverseProperty("TipoDocumentoIdentidad")]
        public virtual ICollection<TblConductor> TblConductor { get; set; }
        [InverseProperty("TipoDocumentoIdentidad")]
        public virtual ICollection<TblGuia> TblGuia { get; set; }
        //[InverseProperty("TipoDocumentoIdentidad")]
        //public virtual ICollection<TblPasajeros> TblPasajeros { get; set; }
        [InverseProperty("TipoDocumentoIdentidad")]
        public virtual ICollection<TblProveedor> TblProveedor { get; set; }
    }
}
