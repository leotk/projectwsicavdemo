﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoWsicav.Models
{
    [Table("Tbl_TipoLicencia")]
    public partial class TblTipoLicencia
    {
        public TblTipoLicencia()
        {
            TblConductor = new HashSet<TblConductor>();
        }

        [Key]
        public int Id { get; set; }
        [StringLength(50)]
        public string Licencia { get; set; }

        [InverseProperty("TipoLicencia")]
        public virtual ICollection<TblConductor> TblConductor { get; set; }
    }
}
