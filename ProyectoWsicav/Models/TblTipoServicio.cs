﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoWsicav.Models
{
    [Table("Tbl_TipoServicio")]
    public partial class TblTipoServicio
    {
        public TblTipoServicio()
        {
            //TblReservasDetalle = new HashSet<TblReservasDetalle>();
        }

        [Key]
        public int Id { get; set; }
        [StringLength(50)]
        public string Descripcion { get; set; }
        public bool Estadotiposervicio { get; set; }

        //[ForeignKey(nameof(TipoServicioEstadoId))]
        //[InverseProperty(nameof(TblTipoServicioEstado.TblTipoServicio))]
        //public virtual TblTipoServicioEstado TipoServicioEstado { get; set; }

        //[InverseProperty("TipoServicio")]
        //public virtual ICollection<TblReservasDetalle> TblReservasDetalle { get; set; }
    }
}
