﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoWsicav.Models
{
    [Table("Tbl_TipoServicioEstado")]
    public partial class TblTipoServicioEstado
    {
        public TblTipoServicioEstado()
        {
            TblTipoServicio = new HashSet<TblTipoServicio>();
        }

        [Key]
        public int Id { get; set; }
        [StringLength(50)]
        public string Estado { get; set; }

        [InverseProperty("TipoServicioEstado")]
        public virtual ICollection<TblTipoServicio> TblTipoServicio { get; set; }
    }
}
