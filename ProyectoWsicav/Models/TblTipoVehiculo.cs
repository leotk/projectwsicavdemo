﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoWsicav.Models
{
    [Table("Tbl_TipoVehiculo")]
    public partial class TblTipoVehiculo
    {
        public TblTipoVehiculo()
        {
            TblVehiculo = new HashSet<TblVehiculo>();
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }
        [StringLength(50)]
        public string TipoVehiculo { get; set; }

        [InverseProperty("TipoVehiculo")]
        public virtual ICollection<TblVehiculo> TblVehiculo { get; set; }
    }
}
