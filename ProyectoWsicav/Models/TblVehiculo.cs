﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoWsicav.Models
{
    [Table("Tbl_Vehiculo")]
    public partial class TblVehiculo
    {
        [Key]
        public int Id { get; set; }
        [StringLength(50)]
        public string Codigo { get; set; }
        public int? TipoVehiculoId { get; set; }
        [StringLength(50)]
        public string Marca { get; set; }
        public int? NumAsientos { get; set; }
        public int? NumMaletas { get; set; }
        [StringLength(150)]
        public string Combustible { get; set; }
        [StringLength(50)]
        public string Placa { get; set; }

        [ForeignKey(nameof(TipoVehiculoId))]
        [InverseProperty(nameof(TblTipoVehiculo.TblVehiculo))]
        public virtual TblTipoVehiculo TipoVehiculo { get; set; }
    }
}
