﻿using Microsoft.AspNetCore.Http;
using ProyectoWsicav.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoWsicav.ViewObject
{
    public class VLicliente
    {
        public VLicliente()
        {
        }
        public VLicliente(TblLiquiCliente lc) //Empleado para editar actualizar
        {
            Id = lc.Id;
            SolicitudId = lc.SolicitudId;
            FechaLcliente = lc.FechaLcliente;
            FormaPago = lc.FormaPago;
            Banco = lc.Banco;
            Titular = lc.Titular;
            NumeTarjeta = lc.NumeTarjeta;
            NumeVouch = lc.NumeVouch;
            Monto = lc.Monto;
            Documentonombre = lc.Documentonombre;
        }

        public int Id { get; set; }
        public int SolicitudId { get; set; }
        [DataType(DataType.Date),Required(ErrorMessage = "Fecha Requerida")]
        public DateTime? FechaLcliente { get; set; }
        public string FormaPago { get; set; }
        public string Banco { get; set; }
        [Required(ErrorMessage = "Nombre de titular")]
        public string Titular { get; set; }
        [Required(ErrorMessage = "N° Tarjeta")]
        public string NumeTarjeta { get; set; } //CORREGIR ESTO EN EL FUTURO POR UN INT EN LA BD
        [Required(ErrorMessage = "Voucher requerido")]
        public string NumeVouch { get; set; }
        [Required(ErrorMessage = "Importe requerido"), RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Monto invalido")]
        public decimal? Monto { get; set; }
        public string Documentonombre { get; set; }
        public IFormFile Doc { get; set; }

    }
}
