﻿
function CotiValidadorFormu(evt) {
    var formulario = document.NuevaCotiza;
    var condicion = true;

    var fcheck = formulario.elements["chek"];
    if (fcheck) {
        if (formulario.chek.checked == true) {
            if (formulario.Item4_DocCoti.value.length != 0 && formulario.validador.value == "false") {
                alert("El archivo debe ser validado");
                condicion = false;
            }
            else if (formulario.Item4_DocCoti.value.length == 0 && formulario.validador.value == "false") {
                alert("No puedes remplazar un archivo existente con uno vacio");
                condicion = false;
            }
        }
        if (condicion) { condicion = confirm('Quieres actualizar el registro?'); }
    } else {
        if (formulario.Item4_DocCoti.value.length != 0 && formulario.validador.value == "false") {
            alert("El archivo debe ser validado");
            condicion = false;
        }
        else if (formulario.Item4_DocCoti.value.length == 0 && formulario.validador.value == "false") {
            alert("No se puede cargar un archivo uno vacio");
            condicion = false;
        }
        if (condicion) { condicion = confirm('Deseas registar esta entrada?'); }
    }

    if (!condicion) {
        if (evt.preventDefault) { event.preventDefault(); }
        else if (evt.returnValue) { evt.returnValue = false; }
        else { return false; }
    }
}

function Coti_cargarFile() {
    document.getElementById("rptCoti").innerHTML = "Cargando archivo, espere .....";

    var formulario = document.getElementsByName("NuevaCotiza")[0]; //PODEMOS OBTENER EL FORMULARIO POR NOMBRE O ID, SI LA 2ND ENTONCES USAR GETELEMENTID

    var key = formulario.elements["__RequestVerificationToken"].value;
    var Doc = formulario.elements["Item4.DocCoti"].files[0];

    var formdata = new FormData();

    formdata.append("Item4.DocCoti", Doc);
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "/Ajax/ValidarArchivo", true);
    xhr.setRequestHeader("RequestVerificationToken", key);
    xhr.send(formdata);

    xhr.onload = function () {
        if (this.status == 200) {
            var rpt = xhr.responseText;
            var rpta = JSON.parse(rpt);
            var span = rpta.span;
            var vali = rpta.bool;

            document.getElementById("rptCoti").innerHTML = span;
            document.getElementById("ValidaCoti").value = vali

        }
    }
}

function ChangeCoti() {
    var formulario = document.NuevaCotiza;
    formulario.validador.value = "false" //validador es el attrb NAME
    formulario.querySelector('[name="respuesta"]').innerHTML = " "; //BUSCA POR EL NOMBRE
}


function FactValidadorFormu(evt) {
    var formulario = document.NuevaFac;
    var condicion = true;

    var fcheck = formulario.elements["chek"];
    if (fcheck) {
        if (formulario.chek.checked == true) {
            if (formulario.Item7_DocFact.value.length != 0 && formulario.validador.value == "false") {
                alert("El archivo debe ser validado");
                condicion = false;
            }
            else if (formulario.Item7_DocFact.value.length == 0 && formulario.validador.value == "false") {
                alert("No puedes remplazar un archivo existente con uno vacio");
                condicion = false;
            }
        }
        if (condicion) { condicion = confirm('Quieres actualizar el registro?'); }
    } else {
        if (formulario.Item7_DocFact.value.length != 0 && formulario.validador.value == "false") {
            alert("El archivo debe ser validado");
            condicion = false;
        }
        else if (formulario.Item7_DocFact.value.length == 0 && formulario.validador.value == "false") {
            alert("No se puede cargar un archivo uno vacio");
            condicion = false;
        }
        if (condicion) { condicion = confirm('Deseas registar esta entrada?'); }
    }

    if (!condicion) {
        if (evt.preventDefault) { event.preventDefault(); }
        else if (evt.returnValue) { evt.returnValue = false; }
        else { return false; }
    }
}

function Fact_cargarFile() {
    document.getElementById("rptFact").innerHTML = "Cargando archivo, espere .....";

    var formulario = document.getElementsByName("NuevaFac")[0]; //PODEMOS OBTENER EL FORMULARIO POR NOMBRE O ID, SI LA 2ND ENTONCES USAR GETELEMENTID

    var key = formulario.elements["__RequestVerificationToken"].value;
    var Doc = formulario.elements["Item7.DocFact"].files[0];

    var formdata = new FormData();

    formdata.append("Item7.DocFact", Doc);
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "/Ajax/ValidarArchivo", true);
    xhr.setRequestHeader("RequestVerificationToken", key);
    xhr.send(formdata);

    xhr.onload = function () {
        if (this.status == 200) {
            var rpt = xhr.responseText;
            var rpta = JSON.parse(rpt);
            var span = rpta.span;
            var vali = rpta.bool;

            document.getElementById("rptFact").innerHTML = span;
            document.getElementById("ValidaFact").value = vali

        }
    }
}

function ChangeFact() {
    var formulario = document.NuevaFac;
    formulario.validador.value = "false" //validador es el attrb NAME
    formulario.querySelector('[name="respuesta"]').innerHTML = " "; //BUSCA POR EL NOMBRE
}



function LcliValidadorFormu(evt) {
    var formulario = document.NuevaLCliente;
    var condicion = true;

    var fcheck = formulario.elements["chek"];
    if (fcheck) {
        if (formulario.chek.checked == true) {
            if (formulario.Item5_Doc.value.length != 0 && formulario.validador.value == "false") {
                alert("El archivo debe ser validado");
                condicion = false;
            }
            else if (formulario.Item5_Doc.value.length == 0 && formulario.validador.value == "false") {
                alert("No puedes remplazar un archivo existente con uno vacio");
                condicion = false;
            }
        }
        if (condicion) { condicion = confirm('Quieres actualizar el registro?'); }
    } else {
        if (formulario.Item5_Doc.value.length != 0 && formulario.validador.value == "false") {
            alert("El archivo debe ser validado");
            condicion = false;
        }
        else if (formulario.Item5_Doc.value.length == 0 && formulario.validador.value == "false") {
            alert("No se puede cargar un archivo uno vacio");
            condicion = false;
        }
        if (condicion) { condicion = confirm('Deseas registar esta entrada?'); }
    }

    if (!condicion) {
        if (evt.preventDefault) { event.preventDefault(); }
        else if (evt.returnValue) { evt.returnValue = false; }
        else { return false; }
    }
}

function Lcli_cargarFile() {
    document.getElementById("rptLcli").innerHTML = "Cargando archivo, espere .....";

    var formulario = document.getElementsByName("NuevaLCliente")[0];

    var key = formulario.elements["__RequestVerificationToken"].value;
    var Doc = formulario.elements["Item5.Doc"].files[0];

    var formdata = new FormData();

    formdata.append("Item5.Doc", Doc);
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "/Ajax/ValidarArchivo", true);
    xhr.setRequestHeader("RequestVerificationToken", key);
    xhr.send(formdata);

    xhr.onload = function () {
        if (this.status == 200) {
            var rpt = xhr.responseText;
            var rpta = JSON.parse(rpt);
            var span = rpta.span;
            var vali = rpta.bool;

            document.getElementById("rptLcli").innerHTML = span;
            document.getElementById("ValidaLcli").value = vali

        }
    }
}

function ChangeLcli() {
    var formulario = document.NuevaLCliente;
    formulario.validador.value = "false" //validador es el attrb NAME
    formulario.querySelector('[name="respuesta"]').innerHTML = " "; //BUSCA POR EL NOMBRE
}



function LproValidadorFormu(evt) {
    var formulario = document.NuevaLProveedor;
    var condicion = true;

    var fcheck = formulario.elements["chek"];
    if (fcheck) {
        if (formulario.chek.checked == true) {
            if (formulario.Item6_Doc.value.length != 0 && formulario.validador.value == "false") {
                alert("El archivo debe ser validado");
                condicion = false;
            }
            else if (formulario.Item6_Doc.value.length == 0 && formulario.validador.value == "false") {
                alert("No puedes remplazar un archivo existente con uno vacio");
                condicion = false;
            }
        }
        if (condicion) { condicion = confirm('Quieres actualizar el registro?'); }
    } else {
        if (formulario.Item6_Doc.value.length != 0 && formulario.validador.value == "false") {
            alert("El archivo debe ser validado");
            condicion = false;
        }
        else if (formulario.Item6_Doc.value.length == 0 && formulario.validador.value == "false") {
            alert("No se puede cargar un archivo uno vacio");
            condicion = false;
        }
        if (condicion) { condicion = confirm('Deseas registar esta entrada?'); }
    }

    if (!condicion) {
        if (evt.preventDefault) { event.preventDefault(); }
        else if (evt.returnValue) { evt.returnValue = false; }
        else { return false; }
    }
}

function Lpro_cargarFile() {
    document.getElementById("rptLpro").innerHTML = "Cargando archivo, espere .....";

    var formulario = document.getElementsByName("NuevaLProveedor")[0];

    var key = formulario.elements["__RequestVerificationToken"].value;
    var Doc = formulario.elements["Item6.Doc"].files[0];

    var formdata = new FormData();

    formdata.append("Item6.Doc", Doc);
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "/Ajax/ValidarArchivo", true);
    xhr.setRequestHeader("RequestVerificationToken", key);
    xhr.send(formdata);

    xhr.onload = function () {
        if (this.status == 200) {
            var rpt = xhr.responseText;
            var rpta = JSON.parse(rpt);
            var span = rpta.span;
            var vali = rpta.bool;

            document.getElementById("rptLpro").innerHTML = span;
            document.getElementById("ValidaLpro").value = vali

        }
    }
}

function ChangeLpro() {
    var formulario = document.NuevaLProveedor;
    formulario.validador.value = "false" //validador es el attrb NAME
    formulario.querySelector('[name="respuesta"]').innerHTML = " "; //BUSCA POR EL NOMBRE
}

//COTIZACION
function CheckCoti() {
    var check = document.getElementById("Coti_Change"); //Anteponer el nombre del modelo_change
    var ifile = document.getElementById("Item4_DocCoti"); //NombreUnico Asignado al input file del IformFile ViewModel
    var nombrepdf = document.getElementById("Item4_Documentonombre"); //NombreUnico para el campo del nombre del pdf a mostrar en el edit

    if (check.checked == true) {
        ifile.style.display = "block"
        nombrepdf.style.display = "none"
    } else {
        ifile.style.display = "none";
        nombrepdf.style.display = "block"
    }
}

//FACTURA
function CheckFact() {
    var check = document.getElementById("Fact_Change"); //Anteponer el nombre del modelo_change
    var ifile = document.getElementById("Item7_DocFact"); //NombreUnico Asignado al input file del IformFile ViewModel
    var nombrepdf = document.getElementById("Item7_Documentonombre"); //NombreUnico para el campo del nombre del pdf a mostrar en el edit
    var boton = document.getElementById("Fact_bcargar"); //Anteponer el nombre del modelo_bcargar

    if (check.checked == true) {
        ifile.style.display = "block"
        boton.style.display = "block"
        nombrepdf.style.display = "none"
    } else {
        ifile.style.display = "none";
        boton.style.display = "none"
        nombrepdf.style.display = "block"
    }
}

//LIQUIDACION CLIENTE
function CheckLcli() {
    var check = document.getElementById("Lcli_Change");
    var ifile = document.getElementById("Item5_Doc");
    var nombrepdf = document.getElementById("Item5_Documentonombre");
    var boton = document.getElementById("Lcli_bcargar");

    if (check.checked == true) {
        ifile.style.display = "block"
        boton.style.display = "block"
        nombrepdf.style.display = "none"
    } else {
        ifile.style.display = "none";
        boton.style.display = "none"
        nombrepdf.style.display = "block"
    }
}

//LIQUIDACION PROVEEDOR
function CheckLpro() {
    var check = document.getElementById("Lpro_Change");
    var ifile = document.getElementById("Item6_Doc");
    var nombrepdf = document.getElementById("Item6_Documentonombre");
    var boton = document.getElementById("Lpro_bcargar");

    if (check.checked == true) {
        ifile.style.display = "block"
        boton.style.display = "block"
        nombrepdf.style.display = "none"
    } else {
        ifile.style.display = "none";
        boton.style.display = "none"
        nombrepdf.style.display = "block"
    }
}

