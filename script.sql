
/****** Object:  Schema [HangFire]    Script Date: 17/04/2020 08:19:51 ******/
CREATE SCHEMA [HangFire]
GO
/****** Object:  UserDefinedFunction [dbo].[CantidadPasajeros]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create FUNCTION [dbo].[CantidadPasajeros] (@idPax int)
RETURNS int
AS
BEGIN
 declare @npax int
 set @npax = 0
 select @npax =  count(id) from Tbl_Pasajeros where solicitudid = @idPax
 RETURN @npax
END
GO
/****** Object:  Table [dbo].[Tbl_CorreoCampana]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
CREATE TABLE [dbo].[Tbl_CorreoCampana](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Asunto] [nchar](300) NULL,
	[Desde] [datetime] NOT NULL,
	[Hasta] [datetime] NOT NULL,
	[Lunes] [bit] NOT NULL,
	[Martes] [bit] NOT NULL,
	[Miercoles] [bit] NOT NULL,
	[Jueves] [bit] NOT NULL,
	[Viernes] [bit] NOT NULL,
	[Sabado] [bit] NOT NULL,
	[Domingo] [bit] NOT NULL,
	[CorreoPlantillaId] [int] NOT NULL,
	[CorreoEmpresaId] [int] NOT NULL,
	[Hora1] [nchar](10) NULL,
	[Hora2] [nchar](10) NULL,
	[Hora3] [nchar](10) NULL,
	[Activo] [bit] NOT NULL,
 CONSTRAINT [PK_CorreoCampaña] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbs_CorreoClienteCampanaPertenece]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbs_CorreoClienteCampanaPertenece](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClienteID] [int] NULL,
	[CampanaID] [int] NULL,
 CONSTRAINT [PK_Tbs_CorreoClienteCampanaPertenece] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Cliente]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Cliente](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Codigo] [char](20) NULL,
	[TipoClienteId] [int] NULL,
	[nombre] [varchar](150) NULL,
	[TipoDocumentoIdentidadId] [int] NULL,
	[Documento] [varchar](20) NULL,
	[Direccion] [varchar](250) NULL,
	[Telefono] [varchar](100) NULL,
	[Email] [varchar](100) NULL,
	[fechaNacimiento] [date] NULL,
	[Principal] [bit] NULL,
	[Celular] [nvarchar](50) NULL,
	[ClienteProvieneID] [int] NULL,
 CONSTRAINT [PK_Tbl_Cliente] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[viw_ClientePerteneceCampana]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[viw_ClientePerteneceCampana]
AS
SELECT        dbo.Tbs_CorreoClienteCampanaPertenece.Id, dbo.Tbs_CorreoClienteCampanaPertenece.ClienteID, dbo.Tbs_CorreoClienteCampanaPertenece.CampanaID, dbo.Tbl_Cliente.nombre, dbo.Tbl_Cliente.Direccion, 
                         dbo.Tbl_Cliente.Telefono, dbo.Tbl_Cliente.Email
FROM            dbo.Tbs_CorreoClienteCampanaPertenece INNER JOIN
                         dbo.Tbl_CorreoCampana ON dbo.Tbs_CorreoClienteCampanaPertenece.CampanaID = dbo.Tbl_CorreoCampana.Id INNER JOIN
                         dbo.Tbl_Cliente ON dbo.Tbs_CorreoClienteCampanaPertenece.ClienteID = dbo.Tbl_Cliente.Id
GO
/****** Object:  Table [dbo].[Tbl_TipoServicio]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_TipoServicio](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](50) NULL,
	[TipoServicioEstadoId] [int] NULL,
 CONSTRAINT [PK_Tbl_TipoServicio] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_ReservasDetalle]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_ReservasDetalle](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SolicitudId] [int] NOT NULL,
	[FechaIn] [datetime] NULL,
	[FechaOut] [datetime] NULL,
	[TipoServicioId] [int] NOT NULL,
	[Localizador] [varchar](50) NULL,
	[FechaLimite] [datetime] NULL,
	[ProveedorId] [int] NOT NULL,
	[Monto] [decimal](18, 2) NULL,
	[Adicional] [bit] NOT NULL,
	[GuiaId] [int] NOT NULL,
	[VehiculoId] [int] NOT NULL,
 CONSTRAINT [PK_Tbl_ReservasDetalle] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Pasajeros]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Pasajeros](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SolicitudId] [int] NULL,
	[NombresPasajeros] [varchar](50) NULL,
	[TipoPasajeroId] [int] NULL,
	[Edad] [int] NULL,
	[TipoDocumentoIdentidadId] [int] NULL,
	[NumeroDucumento] [varchar](50) NULL,
	[Nacionalidad] [varchar](50) NULL,
	[Principal] [bit] NOT NULL,
 CONSTRAINT [PK_Tbl_Pasajeros] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Proveedor]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Proveedor](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Codigo] [varchar](20) NULL,
	[Nombre] [varchar](150) NULL,
	[TipoDocumentoIdentidadId] [int] NULL,
	[Documento] [varchar](20) NULL,
	[Direccion] [varchar](50) NULL,
	[Telefono] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
	[FechaNacimiento] [date] NULL,
 CONSTRAINT [PK_Tbl_Proveedor] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Solicitud]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Solicitud](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NumeroSolicitud] [varchar](50) NULL,
	[FechaSolicitud] [datetime] NULL,
	[UsuarioId] [int] NULL,
	[DestinoId] [int] NULL,
	[Boleto_aereo] [bit] NULL,
	[Hotel] [bit] NULL,
	[Traslado] [bit] NULL,
	[Excursion] [bit] NULL,
	[Guias] [bit] NULL,
	[Seguro_viajes] [bit] NULL,
	[Alquiler_carro] [bit] NULL,
	[FechaIN] [datetime] NULL,
	[FechaOUT] [datetime] NULL,
	[Hora_vuelo_ida] [varchar](10) NULL,
	[Hora_vuelo_regreso] [varchar](10) NULL,
	[Persona_contacto] [varchar](100) NULL,
	[Telefono_contacto] [varchar](50) NULL,
	[Correo_contacto] [varchar](50) NULL,
	[ClienteId] [int] NULL,
	[EstatusId] [int] NULL,
	[Descripcion] [varchar](max) NULL,
	[VendedorId] [int] NULL,
	[Cntadulto] [int] NULL,
	[Cntnino] [int] NULL,
	[Cntinfa] [int] NULL,
	[Idioma] [varchar](50) NULL,
	[Solicitadode] [varchar](50) NULL,
	[Mediollegada] [varchar](50) NULL,
	[Nomfile] [varchar](50) NULL,
	[Estadofile] [int] NULL,
	[Cancelaestado] [bit] NULL,
	[Fechacancelacion] [datetime] NULL,
	[Reaperturaestado] [bit] NULL,
 CONSTRAINT [PK_Tbl_Solicitud] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[viw_Biblia]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[viw_Biblia]
AS
SELECT        s.Id, pax.NombresPasajeros, s.NumeroSolicitud AS Files, dbo.CantidadPasajeros(s.Id) AS nPasajeros, s.Idioma, cl.nombre AS Agencia, '' AS hora, '' AS Servicios, ts.Descripcion AS TipoServicio, 
                         p.Nombre AS Hotel, rd.GuiaId, rd.VehiculoId, '' AS Observaciones, rd.FechaIn, rd.Id AS ReservaDetalleId
FROM            dbo.Tbl_Solicitud AS s LEFT OUTER JOIN
                         dbo.Tbl_Pasajeros AS pax ON s.Id = pax.SolicitudId AND pax.Principal = 1 LEFT OUTER JOIN
                         dbo.Tbl_ReservasDetalle AS rd ON s.Id = rd.SolicitudId LEFT OUTER JOIN
                         dbo.Tbl_TipoServicio AS ts ON rd.TipoServicioId = ts.Id LEFT OUTER JOIN
                         dbo.Tbl_Proveedor AS p ON rd.ProveedorId = p.Id LEFT OUTER JOIN
                         dbo.Tbl_Cliente AS cl ON s.ClienteId = cl.Id
GO
/****** Object:  Table [dbo].[Tbl_Ciudad]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Ciudad](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre_ciudad] [nvarchar](max) NULL,
 CONSTRAINT [PK_Tbl_Ciudad] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_ClienteProviene]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_ClienteProviene](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nchar](50) NOT NULL,
	[Observacion] [nchar](500) NULL,
 CONSTRAINT [PK_Tbl_ClienteProviene] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Conductor]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Conductor](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Codigo] [varchar](50) NULL,
	[TipoDocumentoIdentidadId] [int] NULL,
	[NumDocumento] [int] NULL,
	[Apellidos] [varchar](50) NULL,
	[Nombres] [varchar](50) NULL,
	[Telefono] [int] NULL,
	[Direccion] [varchar](100) NULL,
	[Email] [varchar](100) NULL,
	[TipoLicenciaId] [int] NULL,
 CONSTRAINT [PK_Tbl_Conductor] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Configuracion]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Configuracion](
	[Id] [int] NOT NULL,
	[Clave] [nvarchar](500) NOT NULL,
	[Valor] [nchar](50) NOT NULL,
	[Observacion] [nvarchar](500) NULL,
 CONSTRAINT [PK_Tbl_Configuracion] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_CorreoEmpresa]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_CorreoEmpresa](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Correo] [nvarchar](100) NOT NULL,
	[Password] [nvarchar](100) NOT NULL,
	[SmptHost] [nvarchar](100) NOT NULL,
	[SmptPort] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_Tbl_CorreoEmpresa] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_CorreoPlantillas]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_CorreoPlantillas](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NombrePlantilla] [nchar](50) NOT NULL,
	[CodigoHTML] [nvarchar](max) NOT NULL,
	[Observaciones] [nchar](200) NULL,
 CONSTRAINT [PK_Tbl_CorreoPlantillas] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Cotizacion]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Cotizacion](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SolicitudId] [int] NOT NULL,
	[FechaCotizacion] [datetime] NULL,
	[NumeCotizacion] [varchar](50) NULL,
	[Monto] [decimal](18, 2) NULL,
	[Documentonumero] [nvarchar](50) NULL,
	[Documentonombre] [nvarchar](50) NULL,
	[Documento] [varbinary](max) NULL,
 CONSTRAINT [PK_Tbl_Cotizacion] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Destino]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Destino](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CiudadId] [int] NOT NULL,
	[Nombre] [varchar](100) NULL,
 CONSTRAINT [PK_Tbl_Destino] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Estatus]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Estatus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Estado] [bit] NOT NULL,
	[Color] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Tbl_Estatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Factura]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Factura](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SolicitudId] [int] NOT NULL,
	[FechaFactu] [datetime] NULL,
	[NumeFactu] [nvarchar](20) NULL,
	[Monto] [decimal](18, 2) NULL,
	[Documentonumero] [nvarchar](50) NULL,
	[Documentonombre] [nvarchar](50) NULL,
	[Documento] [varbinary](max) NULL,
 CONSTRAINT [PK_Tbl_Factura] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Guia]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Guia](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Codigo] [varchar](50) NULL,
	[TipoDocumentoIdentidadId] [int] NULL,
	[NumDocumento] [int] NULL,
	[Apellidos] [varchar](100) NULL,
	[Nombres] [varchar](100) NULL,
	[Direccion] [varchar](100) NULL,
	[Telefono] [varchar](10) NULL,
	[Email] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_LiquiCliente]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_LiquiCliente](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SolicitudId] [int] NOT NULL,
	[FechaLCliente] [datetime] NULL,
	[FormaPago] [varchar](50) NULL,
	[Banco] [varchar](50) NULL,
	[Titular] [varchar](100) NULL,
	[NumeTarjeta] [nchar](16) NULL,
	[NumeVouch] [nvarchar](20) NULL,
	[Monto] [decimal](18, 2) NULL,
	[Documentonumero] [nvarchar](50) NULL,
	[Documentonombre] [nvarchar](50) NULL,
	[Documento] [varbinary](max) NULL,
	[estado] [bit] NULL,
 CONSTRAINT [PK_Tbl_LiquiCliente] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO























/****** Object:  Table [dbo].[Tbl_LiquiProve]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_LiquiProve](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SolicitudId] [int] NOT NULL,
	[ProveedorId] [int] NULL,
	[FechaLProve] [datetime] NULL,
	[FormaPago] [varchar](50) NULL,
	[Banco] [varchar](50) NULL,
	[Titular] [varchar](100) NULL,
	[NumeVaucher] [nvarchar](50) NULL,
	[NumeTarjeta] [nchar](16) NULL,
	[PagoCliente] [bit] NULL,
	[Monto] [decimal](18, 2) NULL,
	[Documentonumero] [nvarchar](50) NULL,
	[Documentonombre] [nvarchar](50) NULL,
	[Documento] [varbinary](max) NULL,
 CONSTRAINT [PK_Tbl_LiquiProve] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Permiso]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Permiso](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](50) NULL,
 CONSTRAINT [PK_Tbl_Permiso] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_TipoCliente]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_TipoCliente](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](50) NULL,
 CONSTRAINT [PK_Tbl_TipoCliente] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_TipoDocumentoIdentidad]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_TipoDocumentoIdentidad](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](50) NULL,
 CONSTRAINT [PK_Tbl_TipoDocumentoIdentidad] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_TipoLicencia]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_TipoLicencia](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Licencia] [varchar](50) NULL,
 CONSTRAINT [PK_Tbl_TipoLicencia] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_TipoPasajero]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_TipoPasajero](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Codigo] [varchar](10) NULL,
	[Descripcion] [varchar](50) NULL,
 CONSTRAINT [PK_Tbl_TipoPasajero] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_TipoServicioEstado]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_TipoServicioEstado](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Estado] [varchar](50) NULL,
 CONSTRAINT [PK_Tbl_TipoServicioEstado] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_TipoVehiculo]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_TipoVehiculo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[TipoVehiculo] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Usuario]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Usuario](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[apellido] [varchar](50) NOT NULL,
	[password] [varchar](50) NOT NULL,
	[codigoDepartamento] [char](3) NOT NULL,
	[supervisor] [bit] NOT NULL,
	[email] [varchar](50) NULL,
	[permisoId] [int] NULL,
	[usuario] [varchar](50) NULL,
 CONSTRAINT [PK_Tbl_Usuario] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Vehiculo]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Vehiculo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Codigo] [varchar](50) NULL,
	[TipoVehiculoId] [int] NULL,
	[Marca] [varchar](50) NULL,
	[NumAsientos] [int] NULL,
	[NumMaletas] [int] NULL,
	[Combustible] [varchar](150) NULL,
	[Placa] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_Vendedor]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_Vendedor](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombres] [varchar](100) NULL,
 CONSTRAINT [PK_Tbl_Vendedor] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO






















/****** Object:  Table [HangFire].[AggregatedCounter]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[AggregatedCounter](
	[Key] [nvarchar](100) NOT NULL,
	[Value] [bigint] NOT NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_CounterAggregated] PRIMARY KEY CLUSTERED 
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Counter]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Counter](
	[Key] [nvarchar](100) NOT NULL,
	[Value] [int] NOT NULL,
	[ExpireAt] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [CX_HangFire_Counter]    Script Date: 17/04/2020 08:19:51 ******/
CREATE CLUSTERED INDEX [CX_HangFire_Counter] ON [HangFire].[Counter]
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Hash]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Hash](
	[Key] [nvarchar](100) NOT NULL,
	[Field] [nvarchar](100) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[ExpireAt] [datetime2](7) NULL,
 CONSTRAINT [PK_HangFire_Hash] PRIMARY KEY CLUSTERED 
(
	[Key] ASC,
	[Field] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Job]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Job](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[StateId] [bigint] NULL,
	[StateName] [nvarchar](20) NULL,
	[InvocationData] [nvarchar](max) NOT NULL,
	[Arguments] [nvarchar](max) NOT NULL,
	[CreatedAt] [datetime] NOT NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_Job] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[JobParameter]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[JobParameter](
	[JobId] [bigint] NOT NULL,
	[Name] [nvarchar](40) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_HangFire_JobParameter] PRIMARY KEY CLUSTERED 
(
	[JobId] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[JobQueue]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[JobQueue](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[JobId] [bigint] NOT NULL,
	[Queue] [nvarchar](50) NOT NULL,
	[FetchedAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_JobQueue] PRIMARY KEY CLUSTERED 
(
	[Queue] ASC,
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[List]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[List](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](100) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_List] PRIMARY KEY CLUSTERED 
(
	[Key] ASC,
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Schema]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Schema](
	[Version] [int] NOT NULL,
 CONSTRAINT [PK_HangFire_Schema] PRIMARY KEY CLUSTERED 
(
	[Version] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Server]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Server](
	[Id] [nvarchar](100) NOT NULL,
	[Data] [nvarchar](max) NULL,
	[LastHeartbeat] [datetime] NOT NULL,
 CONSTRAINT [PK_HangFire_Server] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Set]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Set](
	[Key] [nvarchar](100) NOT NULL,
	[Score] [float] NOT NULL,
	[Value] [nvarchar](256) NOT NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_Set] PRIMARY KEY CLUSTERED 
(
	[Key] ASC,
	[Value] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[State]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[State](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[JobId] [bigint] NOT NULL,
	[Name] [nvarchar](20) NOT NULL,
	[Reason] [nvarchar](100) NULL,
	[CreatedAt] [datetime] NOT NULL,
	[Data] [nvarchar](max) NULL,
 CONSTRAINT [PK_HangFire_State] PRIMARY KEY CLUSTERED 
(
	[JobId] ASC,
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_AggregatedCounter_ExpireAt]    Script Date: 17/04/2020 08:19:51 ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_AggregatedCounter_ExpireAt] ON [HangFire].[AggregatedCounter]
(
	[ExpireAt] ASC
)
WHERE ([ExpireAt] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_Hash_ExpireAt]    Script Date: 17/04/2020 08:19:51 ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Hash_ExpireAt] ON [HangFire].[Hash]
(
	[ExpireAt] ASC
)
WHERE ([ExpireAt] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_Job_ExpireAt]    Script Date: 17/04/2020 08:19:51 ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Job_ExpireAt] ON [HangFire].[Job]
(
	[ExpireAt] ASC
)
INCLUDE([StateName]) 
WHERE ([ExpireAt] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_HangFire_Job_StateName]    Script Date: 17/04/2020 08:19:51 ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Job_StateName] ON [HangFire].[Job]
(
	[StateName] ASC
)
WHERE ([StateName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_List_ExpireAt]    Script Date: 17/04/2020 08:19:51 ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_List_ExpireAt] ON [HangFire].[List]
(
	[ExpireAt] ASC
)
WHERE ([ExpireAt] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_Server_LastHeartbeat]    Script Date: 17/04/2020 08:19:51 ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Server_LastHeartbeat] ON [HangFire].[Server]
(
	[LastHeartbeat] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_Set_ExpireAt]    Script Date: 17/04/2020 08:19:51 ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Set_ExpireAt] ON [HangFire].[Set]
(
	[ExpireAt] ASC
)
WHERE ([ExpireAt] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_HangFire_Set_Score]    Script Date: 17/04/2020 08:19:51 ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Set_Score] ON [HangFire].[Set]
(
	[Key] ASC,
	[Score] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tbl_CorreoCampana] ADD  CONSTRAINT [DF_CorreoCampaña_Lunes]  DEFAULT ((0)) FOR [Lunes]
GO
ALTER TABLE [dbo].[Tbl_CorreoCampana] ADD  CONSTRAINT [DF_CorreoCampaña_Martes]  DEFAULT ((0)) FOR [Martes]
GO
ALTER TABLE [dbo].[Tbl_CorreoCampana] ADD  CONSTRAINT [DF_CorreoCampaña_Miercoles]  DEFAULT ((0)) FOR [Miercoles]
GO
ALTER TABLE [dbo].[Tbl_CorreoCampana] ADD  CONSTRAINT [DF_CorreoCampaña_Jueves]  DEFAULT ((0)) FOR [Jueves]
GO
ALTER TABLE [dbo].[Tbl_CorreoCampana] ADD  CONSTRAINT [DF_CorreoCampaña_Viernes]  DEFAULT ((0)) FOR [Viernes]
GO
ALTER TABLE [dbo].[Tbl_CorreoCampana] ADD  CONSTRAINT [DF_CorreoCampaña_Sabado]  DEFAULT ((0)) FOR [Sabado]
GO
ALTER TABLE [dbo].[Tbl_CorreoCampana] ADD  CONSTRAINT [DF_CorreoCampaña_Domingo]  DEFAULT ((0)) FOR [Domingo]
GO
ALTER TABLE [dbo].[Tbl_CorreoCampana] ADD  CONSTRAINT [DF_CorreoCampaña_Activo]  DEFAULT ((1)) FOR [Activo]
GO
ALTER TABLE [dbo].[Tbl_ReservasDetalle] ADD  CONSTRAINT [DF_Tbl_ReservasDetalle_GuiaId]  DEFAULT ((1)) FOR [GuiaId]
GO
ALTER TABLE [dbo].[Tbl_ReservasDetalle] ADD  CONSTRAINT [DF_Tbl_ReservasDetalle_VehiculoId]  DEFAULT ((1)) FOR [VehiculoId]
GO
ALTER TABLE [dbo].[Tbl_Cliente]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_Cliente_Tbl_TipoCliente] FOREIGN KEY([TipoClienteId])
REFERENCES [dbo].[Tbl_TipoCliente] ([Id])
GO
ALTER TABLE [dbo].[Tbl_Cliente] CHECK CONSTRAINT [FK_Tbl_Cliente_Tbl_TipoCliente]
GO
ALTER TABLE [dbo].[Tbl_Cliente]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_Cliente_Tbl_TipoDocumentoIdentidad] FOREIGN KEY([TipoDocumentoIdentidadId])
REFERENCES [dbo].[Tbl_TipoDocumentoIdentidad] ([Id])
GO
ALTER TABLE [dbo].[Tbl_Cliente] CHECK CONSTRAINT [FK_Tbl_Cliente_Tbl_TipoDocumentoIdentidad]
GO
ALTER TABLE [dbo].[Tbl_Conductor]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_Conductor_Tbl_TipoDocumentoIdentidad] FOREIGN KEY([TipoDocumentoIdentidadId])
REFERENCES [dbo].[Tbl_TipoDocumentoIdentidad] ([Id])
GO
ALTER TABLE [dbo].[Tbl_Conductor] CHECK CONSTRAINT [FK_Tbl_Conductor_Tbl_TipoDocumentoIdentidad]
GO
ALTER TABLE [dbo].[Tbl_Conductor]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_Conductor_Tbl_TipoLicencia] FOREIGN KEY([TipoLicenciaId])
REFERENCES [dbo].[Tbl_TipoLicencia] ([Id])
GO
ALTER TABLE [dbo].[Tbl_Conductor] CHECK CONSTRAINT [FK_Tbl_Conductor_Tbl_TipoLicencia]
GO
ALTER TABLE [dbo].[Tbl_CorreoCampana]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_CorreoCampana_Tbl_CorreoEmpresa] FOREIGN KEY([CorreoEmpresaId])
REFERENCES [dbo].[Tbl_CorreoEmpresa] ([Id])
GO
ALTER TABLE [dbo].[Tbl_CorreoCampana] CHECK CONSTRAINT [FK_Tbl_CorreoCampana_Tbl_CorreoEmpresa]
GO
ALTER TABLE [dbo].[Tbl_CorreoCampana]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_CorreoCampana_Tbl_CorreoPlantillas] FOREIGN KEY([CorreoPlantillaId])
REFERENCES [dbo].[Tbl_CorreoPlantillas] ([Id])
GO
ALTER TABLE [dbo].[Tbl_CorreoCampana] CHECK CONSTRAINT [FK_Tbl_CorreoCampana_Tbl_CorreoPlantillas]
GO
ALTER TABLE [dbo].[Tbl_Cotizacion]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_Cotizacion_Tbl_Solicitud] FOREIGN KEY([SolicitudId])
REFERENCES [dbo].[Tbl_Solicitud] ([Id])
GO
ALTER TABLE [dbo].[Tbl_Cotizacion] CHECK CONSTRAINT [FK_Tbl_Cotizacion_Tbl_Solicitud]
GO
ALTER TABLE [dbo].[Tbl_Destino]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_Destino_Tbl_Ciudad] FOREIGN KEY([CiudadId])
REFERENCES [dbo].[Tbl_Ciudad] ([Id])
GO
ALTER TABLE [dbo].[Tbl_Destino] CHECK CONSTRAINT [FK_Tbl_Destino_Tbl_Ciudad]
GO
ALTER TABLE [dbo].[Tbl_Factura]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_Factura_Tbl_Solicitud] FOREIGN KEY([SolicitudId])
REFERENCES [dbo].[Tbl_Solicitud] ([Id])
GO
ALTER TABLE [dbo].[Tbl_Factura] CHECK CONSTRAINT [FK_Tbl_Factura_Tbl_Solicitud]
GO
ALTER TABLE [dbo].[Tbl_Guia]  WITH CHECK ADD FOREIGN KEY([TipoDocumentoIdentidadId])
REFERENCES [dbo].[Tbl_TipoDocumentoIdentidad] ([Id])
GO
ALTER TABLE [dbo].[Tbl_LiquiCliente]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_LiquiCliente_Tbl_Solicitud] FOREIGN KEY([SolicitudId])
REFERENCES [dbo].[Tbl_Solicitud] ([Id])
GO
ALTER TABLE [dbo].[Tbl_LiquiCliente] CHECK CONSTRAINT [FK_Tbl_LiquiCliente_Tbl_Solicitud]
GO
ALTER TABLE [dbo].[Tbl_LiquiProve]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_LiquiProve_Tbl_Proveedor] FOREIGN KEY([ProveedorId])
REFERENCES [dbo].[Tbl_Proveedor] ([Id])
GO
ALTER TABLE [dbo].[Tbl_LiquiProve] CHECK CONSTRAINT [FK_Tbl_LiquiProve_Tbl_Proveedor]
GO
ALTER TABLE [dbo].[Tbl_LiquiProve]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_LiquiProve_Tbl_Solicitud] FOREIGN KEY([SolicitudId])
REFERENCES [dbo].[Tbl_Solicitud] ([Id])
GO
ALTER TABLE [dbo].[Tbl_LiquiProve] CHECK CONSTRAINT [FK_Tbl_LiquiProve_Tbl_Solicitud]
GO
ALTER TABLE [dbo].[Tbl_Pasajeros]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_Pasajeros_Tbl_Solicitud] FOREIGN KEY([SolicitudId])
REFERENCES [dbo].[Tbl_Solicitud] ([Id])
GO
ALTER TABLE [dbo].[Tbl_Pasajeros] CHECK CONSTRAINT [FK_Tbl_Pasajeros_Tbl_Solicitud]
GO
ALTER TABLE [dbo].[Tbl_Pasajeros]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_Pasajeros_Tbl_TipoDocumentoIdentidad] FOREIGN KEY([TipoDocumentoIdentidadId])
REFERENCES [dbo].[Tbl_TipoDocumentoIdentidad] ([Id])
GO
ALTER TABLE [dbo].[Tbl_Pasajeros] CHECK CONSTRAINT [FK_Tbl_Pasajeros_Tbl_TipoDocumentoIdentidad]
GO
ALTER TABLE [dbo].[Tbl_Pasajeros]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_Pasajeros_Tbl_TipoPasajero] FOREIGN KEY([TipoPasajeroId])
REFERENCES [dbo].[Tbl_TipoPasajero] ([Id])
GO
ALTER TABLE [dbo].[Tbl_Pasajeros] CHECK CONSTRAINT [FK_Tbl_Pasajeros_Tbl_TipoPasajero]
GO
ALTER TABLE [dbo].[Tbl_Proveedor]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_Proveedor_Tbl_TipoDocumentoIdentidad] FOREIGN KEY([TipoDocumentoIdentidadId])
REFERENCES [dbo].[Tbl_TipoDocumentoIdentidad] ([Id])
GO
ALTER TABLE [dbo].[Tbl_Proveedor] CHECK CONSTRAINT [FK_Tbl_Proveedor_Tbl_TipoDocumentoIdentidad]
GO
ALTER TABLE [dbo].[Tbl_ReservasDetalle]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_ReservasDetalle_Tbl_Proveedor] FOREIGN KEY([ProveedorId])
REFERENCES [dbo].[Tbl_Proveedor] ([Id])
GO
ALTER TABLE [dbo].[Tbl_ReservasDetalle] CHECK CONSTRAINT [FK_Tbl_ReservasDetalle_Tbl_Proveedor]
GO
ALTER TABLE [dbo].[Tbl_ReservasDetalle]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_ReservasDetalle_Tbl_Solicitud] FOREIGN KEY([SolicitudId])
REFERENCES [dbo].[Tbl_Solicitud] ([Id])
GO
ALTER TABLE [dbo].[Tbl_ReservasDetalle] CHECK CONSTRAINT [FK_Tbl_ReservasDetalle_Tbl_Solicitud]
GO
ALTER TABLE [dbo].[Tbl_ReservasDetalle]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_ReservasDetalle_Tbl_TipoServicio] FOREIGN KEY([TipoServicioId])
REFERENCES [dbo].[Tbl_TipoServicio] ([Id])
GO
ALTER TABLE [dbo].[Tbl_ReservasDetalle] CHECK CONSTRAINT [FK_Tbl_ReservasDetalle_Tbl_TipoServicio]
GO
ALTER TABLE [dbo].[Tbl_Solicitud]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_Solicitud_Tbl_Cliente] FOREIGN KEY([ClienteId])
REFERENCES [dbo].[Tbl_Cliente] ([Id])
GO
ALTER TABLE [dbo].[Tbl_Solicitud] CHECK CONSTRAINT [FK_Tbl_Solicitud_Tbl_Cliente]
GO
ALTER TABLE [dbo].[Tbl_Solicitud]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_Solicitud_Tbl_Destino] FOREIGN KEY([DestinoId])
REFERENCES [dbo].[Tbl_Destino] ([Id])
GO
ALTER TABLE [dbo].[Tbl_Solicitud] CHECK CONSTRAINT [FK_Tbl_Solicitud_Tbl_Destino]
GO
ALTER TABLE [dbo].[Tbl_Solicitud]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_Solicitud_Tbl_Estatus] FOREIGN KEY([EstatusId])
REFERENCES [dbo].[Tbl_Estatus] ([Id])
GO
ALTER TABLE [dbo].[Tbl_Solicitud] CHECK CONSTRAINT [FK_Tbl_Solicitud_Tbl_Estatus]
GO
ALTER TABLE [dbo].[Tbl_Solicitud]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_Solicitud_Tbl_Usuario] FOREIGN KEY([UsuarioId])
REFERENCES [dbo].[Tbl_Usuario] ([Id])
GO
ALTER TABLE [dbo].[Tbl_Solicitud] CHECK CONSTRAINT [FK_Tbl_Solicitud_Tbl_Usuario]
GO
ALTER TABLE [dbo].[Tbl_Solicitud]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_Solicitud_Tbl_Vendedor] FOREIGN KEY([VendedorId])
REFERENCES [dbo].[Tbl_Vendedor] ([Id])
GO
ALTER TABLE [dbo].[Tbl_Solicitud] CHECK CONSTRAINT [FK_Tbl_Solicitud_Tbl_Vendedor]
GO
ALTER TABLE [dbo].[Tbl_TipoServicio]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_TipoServicio_Tbl_TipoServicioEstado] FOREIGN KEY([TipoServicioEstadoId])
REFERENCES [dbo].[Tbl_TipoServicioEstado] ([Id])
GO
ALTER TABLE [dbo].[Tbl_TipoServicio] CHECK CONSTRAINT [FK_Tbl_TipoServicio_Tbl_TipoServicioEstado]
GO
ALTER TABLE [dbo].[Tbl_Usuario]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_Usuario_Tbl_Permiso] FOREIGN KEY([permisoId])
REFERENCES [dbo].[Tbl_Permiso] ([Id])
GO
ALTER TABLE [dbo].[Tbl_Usuario] CHECK CONSTRAINT [FK_Tbl_Usuario_Tbl_Permiso]
GO
ALTER TABLE [dbo].[Tbl_Vehiculo]  WITH CHECK ADD FOREIGN KEY([TipoVehiculoId])
REFERENCES [dbo].[Tbl_TipoVehiculo] ([id])
GO
ALTER TABLE [dbo].[Tbs_CorreoClienteCampanaPertenece]  WITH CHECK ADD  CONSTRAINT [FK_Tbs_CorreoClienteCampanaPertenece_Tbl_Cliente] FOREIGN KEY([ClienteID])
REFERENCES [dbo].[Tbl_Cliente] ([Id])
GO
ALTER TABLE [dbo].[Tbs_CorreoClienteCampanaPertenece] CHECK CONSTRAINT [FK_Tbs_CorreoClienteCampanaPertenece_Tbl_Cliente]
GO
ALTER TABLE [dbo].[Tbs_CorreoClienteCampanaPertenece]  WITH CHECK ADD  CONSTRAINT [FK_Tbs_CorreoClienteCampanaPertenece_Tbl_CorreoCampana1] FOREIGN KEY([CampanaID])
REFERENCES [dbo].[Tbl_CorreoCampana] ([Id])
GO
ALTER TABLE [dbo].[Tbs_CorreoClienteCampanaPertenece] CHECK CONSTRAINT [FK_Tbs_CorreoClienteCampanaPertenece_Tbl_CorreoCampana1]
GO
ALTER TABLE [HangFire].[JobParameter]  WITH CHECK ADD  CONSTRAINT [FK_HangFire_JobParameter_Job] FOREIGN KEY([JobId])
REFERENCES [HangFire].[Job] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [HangFire].[JobParameter] CHECK CONSTRAINT [FK_HangFire_JobParameter_Job]
GO
ALTER TABLE [HangFire].[State]  WITH CHECK ADD  CONSTRAINT [FK_HangFire_State_Job] FOREIGN KEY([JobId])
REFERENCES [HangFire].[Job] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [HangFire].[State] CHECK CONSTRAINT [FK_HangFire_State_Job]
GO
/****** Object:  StoredProcedure [dbo].[prueba]    Script Date: 17/04/2020 08:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[prueba] @id int
as
select S.Id, (case when P.Principal=1 then P.NombresPasajeros else ' ' end) as Pasajero,
SUM(S.Cntnino+S.Cntinfa+S.Cntadulto) as Pasajeros, S.NumeroSolicitud, C.nombre, TS.Descripcion, S.Mediollegada 
from Tbl_Solicitud as S 
inner join Tbl_Pasajeros as P on S.Id=P.SolicitudId 
inner join Tbl_Cliente as C on S.ClienteId=C.Id 
inner join Tbl_ReservasDetalle as RD on P.SolicitudId=RD.SolicitudId 
inner join Tbl_TipoServicio as TS on RD.TipoServicioId=TS.Id where P.TipoPasajeroId=1 and S.Id=@id
GROUP BY S.Id, P.NombresPasajeros, S.NumeroSolicitud, C.nombre, TS.Descripcion, S.Mediollegada, P.Principal
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[47] 4[22] 2[14] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -192
         Left = 0
      End
      Begin Tables = 
         Begin Table = "s"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 247
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pax"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 274
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "rd"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 400
               Right = 247
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ts"
            Begin Extent = 
               Top = 402
               Left = 38
               Bottom = 498
               Right = 247
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "p"
            Begin Extent = 
               Top = 498
               Left = 38
               Bottom = 628
               Right = 274
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cl"
            Begin Extent = 
               Top = 630
               Left = 38
               Bottom = 760
               Right = 274
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Wid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'viw_Biblia'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'th = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 4440
         Alias = 3975
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'viw_Biblia'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'viw_Biblia'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[21] 2[14] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Tbs_CorreoClienteCampanaPertenece"
            Begin Extent = 
               Top = 31
               Left = 354
               Bottom = 144
               Right = 563
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Tbl_CorreoCampana"
            Begin Extent = 
               Top = 41
               Left = 605
               Bottom = 171
               Right = 814
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Tbl_Cliente"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 274
            End
            DisplayFlags = 280
            TopColumn = 9
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'viw_ClientePerteneceCampana'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'viw_ClientePerteneceCampana'
GO
USE [master]
GO
ALTER DATABASE [dbwsicavprueba] SET  READ_WRITE 
GO
